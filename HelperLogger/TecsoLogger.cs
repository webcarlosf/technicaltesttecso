﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLogger
{
    public class TecsoLogger : ITecsoLogger
    {
        public TecsoLogger()
        {

        }

        public void LogMessage(string message, bool logInfo, 
            bool logWarning, bool logError)
        {
            message.Trim();
            if (message == null || message.Length == 0)
            {
                return;
            }
            if (!logInfo && !logWarning && !logError)
            {
                throw new Exception("Especificar tipo de mensaje");
            }

            string typeMessage = "";
            if (logInfo)
            {
                typeMessage = " INFO ";
            }
            else if (logWarning)
            {
                typeMessage = " ALERTA ";
            }
            else if (logError)
            {
                typeMessage = " ERROR ";
            }

            LogMessgeDataBase(message, typeMessage);

            LogMessgeFile(message, typeMessage);

            LogMessgeConsole(message, typeMessage);
        }

        internal static void LogMessgeDataBase(string message, string typeMessage)
        {
            /*
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            connection.Open();

            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("Insert into Log Values('" + message + "', " + t.ToString() + ")");
            command.ExecuteNonQuery();
            */
        }

        internal static void LogMessgeFile(string message, string typeMessage)
        {
            string messageComplete = "";
            if (System.IO.File.Exists(System.Configuration.ConfigurationManager.AppSettings["FilePathLog"] + "Archivo_Log" + DateTime.Now.ToShortDateString() + ".txt"))
            {
                messageComplete = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["FilePathLog"] + "Archivo_Log" + DateTime.Now.ToShortDateString() + ".txt");
            }
            messageComplete = messageComplete + "  " + typeMessage + DateTime.Now.ToShortDateString() + message;

            System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["FilePathLog"] + "Archivo_Log" + DateTime.Now.ToShortDateString() + ".txt", messageComplete);
            
        }

        internal static void LogMessgeConsole(string message, string typeMessage)
        {
            Console.WriteLine(DateTime.Now.ToShortDateString() + message);
        }

        public static TecsoLogger ObtenerInstancia()
        {
            return new TecsoLogger();
        }

    }
}
