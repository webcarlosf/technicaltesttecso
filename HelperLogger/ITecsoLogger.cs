﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLogger
{
    public interface ITecsoLogger
    {
        void LogMessage(string message, bool logInfo, bool logWarning, bool logError);

    }
}
