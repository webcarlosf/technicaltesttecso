﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.Negocio.Curso.Broker;

namespace SistemaAC.Negocio.Curso.Fachada
{
    public class CursoFachada : ICursoFachada
    {
        private CursoBroker broker;

        public CursoFachada(ApplicationDbContext context)
        {
            broker = new CursoBroker(context);
        }

        public List<IdentityError> agregarCurso(int id, string nombre, string descripcion, byte creditos, byte horas, decimal costos, bool estado, int categoria, string funcion)
        {
            return broker.agregarCurso(id, nombre, descripcion, creditos, horas, costos, estado, categoria, funcion);
        }

        public List<IdentityError> editarCurso(int id, string nombre, string descripcion, byte creditos, byte horas, decimal costo, bool estado, int categoriaID, int funcion)
        {
            return broker.editarCurso(id, nombre, descripcion, creditos, horas, costo, estado, categoriaID, funcion);
        }

        public int[] estadosCursos()
        {
            return broker.estadosCursos();
        }

        public List<object[]> filtrarCurso(int numPagina, string valor, string order, int funcion)
        {
            return broker.filtrarCurso(numPagina, valor, order, funcion);
        }

        public List<Models.Categoria> getCategoria(int id)
        {
            return broker.getCategoria(id);
        }

        public List<Models.Categoria> getCategorias()
        {
            return broker.getCategorias();
        }

        public List<Models.Curso> getCursos(int id)
        {
            return broker.getCursos(id);
        }

        public List<Instructor> getInstructors()
        {
            return broker.getInstructors();
        }

        public string getInstructorsCurso(int cursoID)
        {
            throw new NotImplementedException();
        }

        public List<IdentityError> instructorCurso(List<Asignacion> asignacion)
        {
            return broker.instructorCurso(asignacion);
        }
    }
}
