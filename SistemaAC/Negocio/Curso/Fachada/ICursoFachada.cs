﻿using Microsoft.AspNetCore.Identity;
using SistemaAC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaAC.Negocio.Curso.Fachada
{
    public interface ICursoFachada
    {
        List<SistemaAC.Models.Categoria> getCategorias();

        List<SistemaAC.Models.Categoria> getCategoria(int id);

        List<SistemaAC.Models.Curso> getCursos(int id);

        List<IdentityError> agregarCurso(int id, string nombre, string descripcion, byte creditos, byte horas, decimal costos, Boolean estado, int categoria, string funcion);

        List<object[]> filtrarCurso(int numPagina, string valor, string order, int funcion);

        List<IdentityError> editarCurso(int id, string nombre, string descripcion, byte creditos, byte horas, decimal costo, Boolean estado, int categoriaID, int funcion);

        String getInstructorsCurso(int cursoID);

        List<IdentityError> instructorCurso(List<SistemaAC.Models.Asignacion> asignacion);

        List<SistemaAC.Models.Instructor> getInstructors();

        int[] estadosCursos();

    }
}
