﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaAC.Negocio.Inscripciones.Fachada
{
    public interface IInscripciones
    {
        String filtrarEstudiantes(string valor);

        List<IdentityError> guardarCursos(List<SistemaAC.Models.Inscripcion> listCursos);

        List<SistemaAC.Models.Curso> getCursos(int id);

        string filtrarCurso(string valor);

        List<SistemaAC.Models.Estudiante> getEstudiante(int id);

        String getCategorias(int id);

    }
}
