﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.Negocio.Inscripciones.Broker;

namespace SistemaAC.Negocio.Inscripciones.Fachada
{
    public class IncripcionesFachada : IInscripciones
    {
        private InscripcionesBroker broker;

        public IncripcionesFachada(ApplicationDbContext context)
        {
            broker = new InscripcionesBroker(context);
        }

        public string filtrarCurso(string valor)
        {
            return broker.filtrarCurso(valor);
        }

        public string filtrarEstudiantes(string valor)
        {
            return broker.filtrarEstudiantes(valor);
        }

        public string getCategorias(int id)
        {
            return broker.getCategorias(id);
        }

        public List<Models.Curso> getCursos(int id)
        {
            return broker.getCursos(id);
        }

        public List<Models.Estudiante> getEstudiante(int id)
        {
            return broker.getEstudiante(id);
        }

        public List<IdentityError> guardarCursos(List<Inscripcion> listCursos)
        {
            return broker.guardarCursos(listCursos);
        }
    }
}
