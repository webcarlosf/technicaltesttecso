﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaAC.Negocio.Estudiante.Fachada
{
    public interface IEstudianteFachada
    {
        List<SistemaAC.Models.Estudiante> GetEstudiante(int id);

        List<IdentityError> guardarEstudiante(List<SistemaAC.Models.Estudiante> response, int funcion);

        List<object[]> filtrarEstudiantes(int numPagina, string valor, string order);

        List<IdentityError> deleteEstudiante(int id);

        Task<List<SistemaAC.Models.Estudiante>> GetAllEstudiante();

    }
}
