﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.Negocio.Estudiante.Broker;

namespace SistemaAC.Negocio.Estudiante.Fachada
{
    public class EstudianteFachada : IEstudianteFachada
    {
        private EstudianteBroker broker;

        public EstudianteFachada(ApplicationDbContext context)
        {
            broker = new EstudianteBroker(context);
        }

        public List<IdentityError> guardarEstudiante(List<Models.Estudiante> response, int funcion)
        {
            return broker.guardarEstudiante(response, funcion);
        }

        public async Task<List<Models.Estudiante>> GetAllEstudiante()
        {
            return await broker.GetAllEstudiante();
        }

        public List<IdentityError> deleteEstudiante(int id)
        {
            return broker.deleteEstudiante(id);
        }

        public List<object[]> filtrarEstudiantes(int numPagina, string valor, string order)
        {
            return broker.filtrarEstudiantes(numPagina, valor, order);
        }

        public List<Models.Estudiante> GetEstudiante(int id)
        {
            return broker.getEstudiante(id);
        }
        
        
    }
}
