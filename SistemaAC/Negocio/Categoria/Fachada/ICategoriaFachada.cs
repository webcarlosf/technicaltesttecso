﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaAC.Negocio.Categoria.Fachada
{
    public interface ICategoriaFachada
    {
        List<IdentityError> guardarCategoria(string nombre, string descripcion, string estado);

        List<object[]> filtrarDatos(int numPagina, string valor, string order);

        List<SistemaAC.Models.Categoria> getCategorias(int id);

        List<IdentityError> editarCategoria(int idCategoria, string nombre, string descripcion, Boolean estado, int funcion);

    }
}
