﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.Negocio.Categoria.broker;

namespace SistemaAC.Negocio.Categoria.Fachada
{
    public class CategoriaFachada : ICategoriaFachada
    {
        private CategoriaBroker broker;

        public CategoriaFachada(ApplicationDbContext context)
        {
            broker = new CategoriaBroker(context);
        }

        public List<IdentityError> editarCategoria(int idCategoria, string nombre, string descripcion, bool estado, int funcion)
        {
            return broker.editarCategoria(idCategoria, nombre, descripcion, estado, funcion);
        }

        public List<object[]> filtrarDatos(int numPagina, string valor, string order)
        {
            return broker.filtrarDatos(numPagina, valor, order);
        }

        public List<Models.Categoria> getCategorias(int id)
        {
            return broker.getCategorias(id);
        }

        public List<IdentityError> guardarCategoria(string nombre, string descripcion, string estado)
        {
            return broker.guardarCategoria(nombre, descripcion, estado);
        }
    }
}
