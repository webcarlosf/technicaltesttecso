﻿using Microsoft.AspNetCore.Identity;
using SistemaAC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaAC.Negocio.MisCursos.Fachada
{
    public interface IMisCursosFachada
    {
        List<object[]> filtrarMisCurso(int numPagina, string valor);

        List<IdentityError> actualizarMisCurso(DataCurso model);

        List<Instructor> getMisDocentes(string query);

        List<SistemaAC.Models.Estudiante> getMisEstudiantes(string query);

        List<SistemaAC.Models.Curso> getMisCursos(string query);

        List<SistemaAC.Models.Curso> getCursos(String curso);

        String getCurso(int id);
    }
}
