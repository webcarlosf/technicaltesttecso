﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.Negocio.MisCursos.Broker;

namespace SistemaAC.Negocio.MisCursos.Fachada
{
    public class MisCursosFachada : IMisCursosFachada
    {
        private MisCursosBroker broker;

        public MisCursosFachada(ApplicationDbContext context)
        {
            broker = new MisCursosBroker(context);
        }

        public List<IdentityError> actualizarMisCurso(DataCurso model)
        {
            return broker.actualizarMisCurso(model);
        }

        public List<object[]> filtrarMisCurso(int numPagina, string valor)
        {
            return broker.filtrarMisCurso(numPagina, valor);
        }

        public string getCurso(int id)
        {
            return broker.getCurso(id);
        }

        public List<Models.Curso> getCursos(string curso)
        {
            return broker.getCursos(curso);
        }
        
        public List<Models.Curso> getMisCursos(string query)
        {
            return broker.getMisCursos(query);
        }

        public List<Instructor> getMisDocentes(string query)
        {
            return broker.getMisDocentes(query);
        }

        public List<Models.Estudiante> getMisEstudiantes(string query)
        {
            return broker.getMisEstudiantes(query);
        }
    }
}
