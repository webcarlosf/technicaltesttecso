﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.ModelsClass;
using SistemaAC.Negocio.MisCursos.Fachada;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaAC.Controllers
{
    public class MisCursosController:Controller
    {
        private readonly ApplicationDbContext _context;
        private IMisCursosFachada misCursosFachada;

        public MisCursosController(ApplicationDbContext context)
        {
            _context = context;
            misCursosFachada = new MisCursosFachada(context);
        }

        public IActionResult Index()
        {
            return View();
        }
        public List<object[]> filtrarMisCurso(int numPagina, string valor)
        {
            return misCursosFachada.filtrarMisCurso(numPagina, valor);
        }
        public List<Curso> getMisCursos(String query)
        {
            return misCursosFachada.getMisCursos(query);
        }
        public List<Estudiante> getMisEstudiantes(string query)
        {
            return misCursosFachada.getMisEstudiantes(query);
        }
        public List<Instructor> getMisDocentes(string query)
        {
            return misCursosFachada.getMisDocentes(query);
        }
        public List<IdentityError> actualizarMisCurso(String data)
        {
            var array = JArray.Parse(data);
            var dataCurso = array[0];
            DataCurso model = JsonConvert.DeserializeObject<DataCurso>(dataCurso.ToString());
            return misCursosFachada.actualizarMisCurso(model);
        }
   
    }
}
