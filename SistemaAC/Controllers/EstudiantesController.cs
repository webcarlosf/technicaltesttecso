﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.ModelsClass;
using SistemaAC.Negocio.Estudiante.Fachada;

namespace SistemaAC.Controllers
{
    public class EstudiantesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IEstudianteFachada estudianteFachada;

        public EstudiantesController(ApplicationDbContext context)
        {
            _context = context;
            estudianteFachada = new EstudianteFachada(context);
        }

        // GET: Estudiantes
        public async Task<IActionResult> Index()
        {
            return View(await estudianteFachada.GetAllEstudiante());
        }

        public List<IdentityError> guardarEstudiante(List<Estudiante> response, int funcion)
        {
            return estudianteFachada.guardarEstudiante(response, funcion);
        }

        public List<object[]> filtrarEstudiantes(int numPagina, string valor, string order)
        {
            return estudianteFachada.filtrarEstudiantes(numPagina, valor, order);
        }
        public List<Estudiante> getEstudiante(int id)
        {
            return estudianteFachada.GetEstudiante(id);
        }
        public List<IdentityError> deleteEstudiante(int id)
        {
            return estudianteFachada.deleteEstudiante(id);
        }
                
    }
}
