﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.ModelsClass;
using SistemaAC.Negocio.Curso.Fachada;

namespace SistemaAC.Controllers
{
    public class CursosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ICursoFachada cursoFachada;

        public CursosController(ApplicationDbContext context)
        {
            _context = context;
            cursoFachada = new CursoFachada(context);
        }

        // GET: Cursos
        public async Task<IActionResult> Index()
        {
            return View();
        }
        public List<Categoria> getCategorias()
        {
            return cursoFachada.getCategorias();
        }
        public List<IdentityError> agregarCurso(int id, string nombre, string descripcion, byte creditos, byte horas, decimal costo, Boolean estado, int categoria, string funcion)
        {
            return cursoFachada.agregarCurso(id, nombre, descripcion, creditos, horas, costo, estado, categoria, funcion);
        }
        public List<object[]> filtrarCurso(int numPagina, string valor, string order, int funcion)
        {
            return cursoFachada.filtrarCurso(numPagina, valor, order, funcion);
        }
        public List<Curso> getCursos(int id)
        {
            return cursoFachada.getCursos(id);
        }
        public List<IdentityError> editarCurso(int id, string nombre, string descripcion, byte creditos, byte horas, decimal costo, Boolean estado, int categoria, int funcion)
        {
            return cursoFachada.editarCurso(id, nombre, descripcion, creditos, horas, costo, estado, categoria, funcion);
        }
        public List<Instructor> getInstructors()
        {
            return cursoFachada.getInstructors();
        }
        public List<IdentityError> instructorCurso(List<Asignacion> asignacion)
        {
            return cursoFachada.instructorCurso(asignacion);
        }
    }
}
