﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaAC.Data;
using SistemaAC.Models;
using SistemaAC.ModelsClass;
using SistemaAC.Negocio.Inscripciones.Fachada;

namespace SistemaAC.Controllers
{
    public class Inscripciones : Controller
    {
        private readonly ApplicationDbContext _context;
        private IInscripciones inscripciones;

        public Inscripciones(ApplicationDbContext context)
        {
            _context = context;
            inscripciones = new IncripcionesFachada(context);
        }
        public IActionResult Index()
        {
            return View();
        }
        public String filtrarEstudiantes(string valor)
        {
           return inscripciones.filtrarEstudiantes(valor);
        }
        public List<Estudiante> getEstudiante(int id)
        {
            return inscripciones.getEstudiante(id);
        }
        public String filtrarCurso(string valor)
        {
            return inscripciones.filtrarCurso(valor);
        }
        public List<Curso> getCurso(int id)
        {
            return inscripciones.getCursos(id);
        }
        public List<IdentityError> guardarCursos(List<Inscripcion> listCursos)
        {
            return inscripciones.guardarCursos(listCursos); 
        }
    }
}