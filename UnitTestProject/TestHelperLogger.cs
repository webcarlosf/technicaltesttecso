﻿using HelperLogger;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject
{
    [TestClass]
    public class TestHelperLogger
    {
        [TestMethod]
        public void TestMethodLogMessageInfo()
        {
            try
            {
                ITecsoLogger tecsoLogger;
                tecsoLogger = TecsoLogger.ObtenerInstancia();

                bool logInfo = true;
                bool logWarning = false;
                bool logError = false;
                string messagge = "Test LogInfo";

                tecsoLogger.LogMessage(messagge, logInfo, logWarning, logError);
            }
            catch (Exception)
            {
                Assert.Fail("");
            }

            
        }

    }
}
